# Requirements
* `python 3.7`
* An existing strapi project

# Upgrade Strapi
#### Note: This currently works only if you're using Mongo.
Just run 
```bash
python3.7 index.py path/to/your/strapi/project
```
It will follow strapi upgrade guide by creating a new project and replacing your project's admin and plugins.
Then it will install any missing plugins.
Done :)

Please make sure you are using VCS e.g. `git` to avoid any code lose. Also I am not responsible for any issues this may cause.

Feel free to open PR and/or issues.
import os
import subprocess
import sys
import tempfile
from pathlib import Path
import json

'''
Get all api's (json) settings and save them
Create a new strapi site
add api's using strapi commandline 
Copy attributes from old api to new api
'''
apis = []
configs = []
strapi_package = 'strapi@alpha'


def error_occurred(message):
    print(message)
    exit(0)


def print_help():
    print('Usage: strapi-upgrade [path/to/strapi/folder]')


def installed_strapi_version():
    """
    Returns current installed strapi version
    :return:
    """
    try:
        version = subprocess.check_output(['strapi', 'version'])
    except FileNotFoundError:
        return False

    return version.decode("utf-8").strip()


def goodby():
    error_occurred('Goodby!')


def install_strapi():
    """
    Install/update strapi
    :return:
    """
    print('Updating strapi...')
    subprocess.check_output(['npm', 'install', strapi_package, '-g'])


def initial_check():
    print('Initial check...')
    # Make sure the folder argument was passed
    if len(sys.argv) < 2:
        print_help()
        exit(0)

    global site_path
    site_path = sys.argv[1].rstrip('/')
    # Check if it's a valid DIR
    if not os.path.isdir(site_path):
        error_occurred(f'Error: "{site_path}" is not a directory.')


def get_site_name():
    package = json.loads(Path(f'{site_path}/package.json').read_text())
    return package['name']


def cleanup(new_site_path):
    print('Cleaning up...')
    os.system(f'rm -rf {new_site_path}')


def upgrade_strapi():
    """
    Upgrades strapi following the guide on
    https://github.com/strapi/strapi/wiki/migration-guide-alpha.14.5-to-alpha.15
    :return:
    """
    # install/update strapi
    install_strapi()
    # Update strapi's (project) dependencies
    os.system(f'cd {site_path} && npm install {strapi_package} --save')

    site_name = get_site_name()
    new_site_temp = f'{tempfile.gettempdir()}/{site_name}'
    input(f'Please follow the instructions and don\'t pay attention to DB configurations. Press any key to continue...')
    # Create new site
    os.system(f'cd {tempfile.gettempdir()} && strapi new {site_name}')

    relations_file = f'/plugins/users-permissions/models/User.settings.json'
    subprocess.check_output(['cp', f'{site_path}/{relations_file}', f'{new_site_temp}/{relations_file}'])

    # Check installed plugins
    installed_plugins = os.listdir(f'{site_path}/plugins')
    # Delete old plugins
    subprocess.check_output(['rm', '-rf', f'{site_path}/plugins'])
    # Copy new plugins
    subprocess.check_output(['cp', '-r', f'{new_site_temp}/plugins', f'{site_path}/plugins'])
    # Install missing plugins
    new_plugins = os.listdir(f'{site_path}/plugins')
    for p in installed_plugins:
        if p not in new_plugins:
            os.system(f'cd {site_path} && strapi install {p}')

    # Delete old admin
    subprocess.check_output(['rm', '-rf', f'{site_path}/admin'])
    # Copy new admin
    subprocess.check_output(['cp', '-r', f'{new_site_temp}/admin', f'{site_path}/admin'])

    # Update service
    service_file = 'plugins/content-type-builder/node_modules/strapi-generate-api/templates/bookshelf/service.template'
    subprocess.check_output(['cp', f'{new_site_temp}/{service_file}', f'{site_path}/{service_file}'])

    # cleanup
    cleanup(new_site_temp)


if __name__ == '__main__':
    # Do initial check
    initial_check()
    upgrade_strapi()
